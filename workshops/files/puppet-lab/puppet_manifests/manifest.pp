node default {
# Test message
  notify { "Debug output on ${hostname} node.": }

  # include ntp, git
}

node 'puppet-node1', 'puppet-node2' {

  file { '/tmp/hello.txt':
    ensure => file,
    content => "hello, world\n",
  }

  file { '/tmp/bye.txt':
    ensure => file,
    content => "goodbye, world\n",
  }

  package { 'cowsay':
    ensure => installed,
  }

  service { 'sshd':
    ensure => running,
    enable => true,
  }

# # Test message
  notify { "Debug output on ${hostname} node.": }

#   include ntp, git, docker, fig
}
