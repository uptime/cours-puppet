1. Vérif syntaxe vagrantfile : dans quelle boucle la création de VM est elle autorisée ?
----
# Introduction à Puppet 
## Avantages et inconvénients
Plus structuré qu'Ansible, contraint à se poser des questions. Beaucoup de modules bien faits.
Mais surtout : *réapplique la configuration*.

Puppet 6 : *La configuration standalone est deprecated en puppet 6.*


## Un premier manifeste
```puppet
   file { '/tmp/hello.txt':
     ensure  => file,
     content => "hello, world\n",
}
```

   `sudo puppet apply /examples/file_hello.pp`

      `cat /tmp/hello.txt`

<!-- ## Les ressources
      The code term `file` begins a resource declaration for a file resource. A resource is some bit of configuration that you want Puppet to manage: for example, a file, user account, or package. A resource declaration follows this pattern:
```
 RESOURCE_TYPE { TITLE:
  ATTRIBUTE => VALUE,
  ...
}
```
 -->

## Démonstration de l'écrasement par Puppet
   ```bash
   # This file is managed by Puppet - any manual edits will be lost
   ```

=> ça réapplique les trucs

## Les options noop et show-diff
Adding the --noop flag to puppet apply will show you what Puppet would have done, without actually changing anything:
```bash
sudo sh -c 'echo "goodbye, world" >/tmp/hello.txt'
sudo puppet apply --noop /examples/file_hello.pp
Notice: Compiled catalog for ubuntu-xenial in environment production in 0.04 seconds
Notice: /Stage[main]/Main/File[/tmp/hello.txt]/content: current_value {md5}7678..., should be {md5}22c3... (noop)
```

Testez aussi `--show_diff`.

## La ressource package
 Although every operating system has its own package format, and different formats vary quite a lot in their capabilities, Puppet represents all these possibilities with a single package type. If you specify in your Puppet manifest that a given package should be installed, Puppet will use the appropriate package manager commands to install it on whatever platform it's running on.
 
 The RESOURCE_TYPE is package, and the only attribute you usually need to specify is ensure, and the only value it usually needs to take is installed:
   package { 'cowsay':
     ensure => installed,
}

## L'outil puppet resource:
```puppet
   puppet resource package openssl
   package { 'openssl':
     ensure => '1.0.2g-1ubuntu4.8',
}
```


# Une configuration master-node avec ansible et vagrant

* Un puppet server (master)
* Un ou plusieurs puppet agent (node)

On va créer un rôle à partir de ces instructions : 
https://puppet.com/docs/puppet/6.5/config_important_settings.html#basics

* server : https://puppet.com/docs/puppet/6.6/install_agents.html#configuring-agents
* sourceaddress
* certname
* autosign
https://puppet.com/docs/puppet/6.6/install_agents.html#task-9788



### Scripts Vagrant et ansible
`vagrant up` avec le Vagrantfile et le rôle ansible qui va avec.

### Installer le CA 
/opt/puppetlabs/bin/puppetserver ca setup
      args:
        creates: "/etc/puppetlabs/puppet/ssl/ca"


<!-- ### Copier la config du master et du node
https://github.com/puppetlabs/puppetserver/blob/master/ezbake/config/conf.d/auth.conf -->

common.yml
master.yml
agent.yml
 master_signing.yml


# Installer Wordpress

https://www.digitalocean.com/community/tutorials/how-to-create-a-puppet-module-to-automate-wordpress-installation-on-ubuntu-14-04